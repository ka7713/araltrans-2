#pragma once

#include "MultiPlugin.h"

const TCHAR *GetATDirectory();

void GetATPluginArgsFromOptionString(const char *pszOption, ATPLUGIN_ARGUMENT_ARRAY &aPluginArgs);

void GetOptionStringFromATPluginArgs(const ATPLUGIN_ARGUMENT_ARRAY &aPluginArgs, char *pszOption, int nMaxOptionLength);
