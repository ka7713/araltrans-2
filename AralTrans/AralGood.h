#pragma once

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CAralGoodApp:
// See AralGood.cpp for the implementation of this class
//

class CAralGoodApp : public CWinApp
{
public:
	CAralGoodApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAralGoodApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CAralGoodApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
