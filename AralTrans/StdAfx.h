#pragma once

//#define TEST_VERSION	// 테스트버전 define. 자동 업데이트를 테스트버전에서 임시로 죽이기 위해 사용.
#include <SDKDDKVer.h>

//#define WINVER 0x0500
//#pragma warning(disable:4996)
//#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <winsvc.h>
#include <process.h>
#include <tlhelp32.h>
#include <shlwapi.h>
#include <MMSystem.h>
#include <tlhelp32.h>
#include <Psapi.h>
#include <Userenv.h>
#include <WtsApi32.h>
#include <winternl.h>  
#include <afx.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
namespace fs = boost::filesystem;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>
#include <crtdbg.h>
#endif
