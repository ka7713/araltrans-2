#include "stdafx.h"
#include "Minizip.h"
#include "mz.h"
#include "mz_zip.h"
#include "mz_strm.h"
#include "mz_zip_rw.h"
#include "mz_os.h"

bool minizip_get_file_date(const std::string &_path, time_t *_modified_date, time_t *_accessed_date, time_t *_creation_date)
{
	return mz_os_get_file_date(_path.c_str(), _modified_date, _accessed_date, _creation_date) == MZ_OK;
}

bool minizip_add(const std::string &_zipfile, const std::vector<std::string> &_files, const std::string &_rootPath)
{
	void *writer = NULL;
	int32_t err = MZ_OK;
	int32_t err_close = MZ_OK;

	mz_zip_writer_create(&writer);
	mz_zip_writer_set_compress_method(writer, MZ_COMPRESS_METHOD_DEFLATE);
	mz_zip_writer_set_compress_level(writer, MZ_COMPRESS_LEVEL_DEFAULT);
	mz_zip_writer_set_progress_cb(writer, 0, 0);

	err = mz_zip_writer_open_file(writer, _zipfile.c_str(), 0, 0);

	if (err != MZ_OK)
		return false;

	std::string rootPath = _rootPath;
	if (0 == rootPath.size())
		rootPath = fs::path(_zipfile).branch_path().string();

	for (auto &file : _files)
	{
		err = mz_zip_writer_add_path(writer, file.c_str(), rootPath.c_str(), 0, 1);
		if (err != MZ_OK)
			goto error;
	}

	err_close = mz_zip_writer_close(writer);
	if (err_close != MZ_OK)
		err = err_close;

error:
	mz_zip_writer_delete(&writer);

	return err == MZ_OK;
}

bool minizip_extract(const std::string &_zipfile, const std::string &_destination, const std::function<bool(zip_file_info *file_info, const char *path)> &_overwrite_cb)
{
	void *reader = NULL;
	int32_t err = MZ_OK;
	int32_t err_close = MZ_OK;

	mz_zip_reader_create(&reader);
	mz_zip_reader_set_pattern(reader, NULL, 1);
	mz_zip_reader_set_overwrite_cb(reader, (void*)&_overwrite_cb, [](void *handle, void *userdata, mz_zip_file *file_info, const char *path)->int32_t
	{
		auto cb = (std::function<bool(zip_file_info *file_info, const char *path)>*)userdata;
		if ((*cb)((zip_file_info*)file_info, path))
			return MZ_OK;
		return MZ_EXIST_ERROR;
	});

	err = mz_zip_reader_open_file(reader, _zipfile.c_str());

	if (err != MZ_OK)
		return false;
	else
		err = mz_zip_reader_save_all(reader, _destination.c_str());

	err_close = mz_zip_reader_close(reader);
	if (err_close != MZ_OK)
		err = err_close;

	mz_zip_reader_delete(&reader);

	return err == MZ_OK;
}
