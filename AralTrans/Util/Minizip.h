#pragma once

struct zip_file_info
{
	uint16_t version_madeby;            /* version made by */
	uint16_t version_needed;            /* version needed to extract */
	uint16_t flag;                      /* general purpose bit flag */
	uint16_t compression_method;        /* compression method */
	time_t   modified_date;             /* last modified date in unix time */
	time_t   accessed_date;             /* last accessed date in unix time */
	time_t   creation_date;             /* creation date in unix time */
	uint32_t crc;                       /* crc-32 */
	int64_t  compressed_size;           /* compressed size */
	int64_t  uncompressed_size;         /* uncompressed size */
	uint16_t filename_size;             /* filename length */
	uint16_t extrafield_size;           /* extra field length */
	uint16_t comment_size;              /* file comment length */
	uint32_t disk_number;               /* disk number start */
	int64_t  disk_offset;               /* relative offset of local header */
	uint16_t internal_fa;               /* internal file attributes */
	uint32_t external_fa;               /* external file attributes */

	const char     *filename;           /* filename utf8 null-terminated string */
	const uint8_t  *extrafield;         /* extrafield data */
	const char     *comment;            /* comment utf8 null-terminated string */

	uint16_t zip64;                     /* zip64 extension mode */
	uint16_t aes_version;               /* winzip aes extension if not 0 */
	uint8_t  aes_encryption_mode;       /* winzip aes encryption mode */
};

bool minizip_get_file_date(const std::string &_path, time_t *_modified_date, time_t *_accessed_date = 0, time_t *_creation_date = 0);

bool minizip_add(const std::string &_zipfile, const std::vector<std::string> &_files, const std::string &_rootPath = "");

bool minizip_extract(const std::string &_zipfile, const std::string &_destination, const std::function<bool(zip_file_info *file_info, const char *path)> &_overwrite_cb = nullptr);
