#pragma once
#include <afx.h>

class CRegistryMgr  
{
private:
	CRegistryMgr();
	virtual ~CRegistryMgr();

	static LONG		RegDeleteRecursive(HKEY hKey, LPCTSTR lpSubKey);
	
public:

	static CString	RegRead(CString rpath, CString name);
	static BOOL		RegWrite(CString rpath, CString name, CString value);

	static DWORD	RegReadDWORD(CString rpath, CString name);
	static BOOL		RegWriteDWORD(CString rpath, CString name, DWORD value);

	static size_t	RegReadBINARY(CString rpath, CString name, unsigned char* retValue);
	static BOOL		RegWriteBINARY(CString rpath, CString name, unsigned char* value, size_t len);

	static BOOL		RegExist(CString rpath, CString name);
	static BOOL		RegDelete(CString rpath, CString name);

	static BOOL		RegistFileType(CString strExt, CString strAppPath, CString strIconPath);
	static BOOL		DeleteFileType(CString strExt);

};
