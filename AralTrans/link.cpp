#include "stdafx.h"

#if PSAPI_VERSION == 1
	#pragma comment(lib, "Psapi.lib")
#else
	#pragma comment(lib, "Kernel32.lib")
#endif

#pragma comment(lib, "Userenv.lib")
#pragma comment(lib, "WtsApi32.lib")
#pragma comment(lib,"version.lib")
#pragma comment(lib, "winmm.lib" )
#pragma comment(lib, "zlib-1.2.8-mt-s.lib" )
#pragma comment(lib, "minizip-2.8.1.lib" )
